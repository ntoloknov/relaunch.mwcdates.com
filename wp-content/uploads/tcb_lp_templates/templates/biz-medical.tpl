<div class="tve_lp_header tve_content_width tve_empty_dropzone">
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1">
		<div class="out" style="background-color: #fff;">
			<div class="in darkSec">
				<div class="cck clearfix">
					<div class="thrv_wrapper thrv_columns tve_clearfix"
					     style="margin-top: 0;margin-bottom: 0;">
						<div class="tve_colm tve_oth">
							<h5 style="color: #000; font-size: 24px;margin-top: 0;margin-bottom: 0;" class="bizmedical-headertext">
								<span class="bold_text"><font color="#3366cc">Medical</font></span>
								Practice
							</h5>
						</div>
						<div class="tve_colm tve_oth">
							<h6 class="bizmedical-location-heading tve_p_center bizmedical-headertext"
							    style="color: #333;font-size: 16px;margin-top: 5px;margin-bottom: 0;">
								105 Street Name, 300120
							</h6>
						</div>
						<div class="tve_colm tve_thc tve_lst">
							<h6 class="bizmedical-phone-heading tve_p_right bizmedical-headertext"
							    style="color: #000;font-size:20px;margin-top: 0;margin-bottom: 0;">
								0723 456 789
							</h6>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="tve_lp_content tve_editor_main_content tve_empty_dropzone tve_content_width">
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1">
		<div class="out" style="box-shadow: 0 7px 0 0 #4c8aca;">
			<div class="in darkSec pddbg pdfbg" data-width="1920" data-height="657"
			     style="box-sizing: border-box; background-image: url({tcb_lp_base_url}/css/images/bizmedical-ps.jpg);">
				<div class="cck clearfix">
					<div class="thrv_wrapper thrv_columns tve_clearfix">
						<div class="tve_colm tve_tfo tve_df ">
							<h1 style="color: #333333; font-size: 60px;margin-top: 30px;margin-bottom: 50px;" class="rft">
								<span class="bold_text">Easy Access</span> to <br>
								<span class="bold_text">Advanced</span> Medical Services
							</h1>
							<div class="thrv_wrapper thrv_content_container_shortcode">
								<div class="tve_clear"></div>
								<div class="tve_left tve_content_inner"
								     style="width: 540px;min-width:50px; min-height: 2em;">
									<p style="color: #333; font-size: 18px;margin-top: 0;margin-bottom: 40px;">
										<span class="bold_text">
											Proin gravida nibh vel velit auctor aliquet. Aenean
											sollicitudin,
											lorem quis bibendum auctor, nisi elit consequat ipsum, nec
											sagittis
											sem nibh id elit. Duis sed odio sit amet nibh vulputate
											cursus a sit
											amet mauris. Morbi accumsan ipsum velit.
										</span>
									</p>
								</div>
								<div class="tve_clear"></div>
							</div>
							<div class="thrv_wrapper thrv_content_container_shortcode bizmedical-container">
								<div class="tve_clear"></div>
								<div class="tve_left tve_content_inner"
								     style="width: 440px;min-width:50px; min-height: 2em;">
									<div
										class="thrv_wrapper thrv_button_shortcode tve_fullwidthBtn"
										data-tve-style="1">
										<div
											class="tve_btn tve_blue tve_normalBtn tve_btn7 tve_nb">
											<a class="tve_btnLink" href="">
					                                            <span class="tve_left tve_btn_im">
					                                                <i></i>
					                                            </span>
												<span class="tve_btn_txt">Make an Appointment</span>
											</a>
										</div>
									</div>
									<h5 class="tve_p_center"
									    style="color: #333;font-size: 24px;margin-top: 0;margin-bottom: 0;">
										<span class="bold_text">
											or CALL : 0745 123 456
										</span>
									</h5>
								</div>
								<div class="tve_clear"></div>
							</div>
						</div>
						<div class="tve_colm  tve_foc tve_ofo tve_df tve_lst">
							<p>
								&nbsp;
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1">
		<div class="out" style="background-color: #fff;margin-top: 7px;">
			<div class="in darkSec">
				<div class="cck clearfix">
					<div class="thrv_wrapper thrv_columns" style="margin-bottom: 60px;">
						<div class="tve_colm tve_twc">
							<h2 style="color: #3868c4; font-size: 36px;margin-top: 0;margin-bottom: 30px;" class="rft">
								Medical Excellence
							</h2>
							<p style="color: #333; font-size:18px;margin-top: 0;margin-bottom: 25px;">
								<span class="bold_text">
									Tell them how you got established as a medical office,
									about how you are focusing on bringing a special benefit to the
									patients ( like low waiting times, free consultations etc )
								</span>
							</p>
							<p style="color: #333; font-size:18px;margin-top: 0;margin-bottom: 25px;">
								<span class="bold_text">
									You can also tell them about your main specialty and important
									achievements, like certificates or other titles.
								</span>
							</p>
						</div>
						<div class="tve_colm tve_twc tve_lst">
							<div style="margin-top: 10px;margin-bottom: 0;width: 381px;"
							     class="thrv_wrapper tve_image_caption img_style_framed alignright bizmedical-image">
		                        <span class="tve_image_frame">
		                            <img class="tve_image"
		                                 src="{tcb_lp_base_url}/css/images/bizmedical-image.jpg"
		                                 style="width: 381px"/>
		                        </span>
							</div>
						</div>
					</div>
					<h2 class="tve_p_center rft"
					    style="color: #3868c4;font-size: 36px;margin-top: 0;margin-bottom: 60px;">
						The Best Care You Can Get
					</h2>
					<div class="thrv_wrapper thrv_columns tve_clearfix"
					     style="">
						<div class="tve_colm tve_foc">
							<div class="thrv_wrapper thrv_icon aligncenter"
							     style="font-size: 50px; margin-bottom: 20px;">
								<span data-tve-icon="bizmedical-icon-heart"
								      class="tve_sc_icon bizmedical-icon-heart tve_blue"></span>
							</div>
							<h6 class="tve_p_center"
							    style="color: #333;font-size: 22px;margin-top: 0;margin-bottom: 30px;">
								Best Patient <br>Comfort
							</h6>
							<p class="tve_p_center"
							   style="font-size: 18px;margin-top: 0;margin-bottom: 40px;">
								Trust and safety form de basis of our approach - we greet each
								patient with the utmost esteem
							</p>
						</div>
						<div class="tve_colm tve_foc">
							<div class="thrv_wrapper thrv_icon aligncenter"
							     style="font-size: 50px; margin-bottom: 20px;">
								<span data-tve-icon="bizmedical-icon-hat"
								      class="tve_sc_icon bizmedical-icon-hat tve_blue"></span>
							</div>
							<h6 class="tve_p_center"
							    style="color: #333;font-size: 22px;margin-top: 0;margin-bottom: 30px;">
								High Profile <br>Doctors
							</h6>
							<p class="tve_p_center"
							   style="font-size: 18px;margin-top: 0;margin-bottom: 40px;">
								Each of our team members is trained in a special field of medical
								activity
							</p>
						</div>
						<div class="tve_colm tve_foc">
							<div class="thrv_wrapper thrv_icon aligncenter"
							     style="font-size: 50px; margin-bottom: 20px;">
								<span data-tve-icon="bizmedical-icon-magnifier"
								      class="tve_sc_icon bizmedical-icon-magnifier tve_blue"></span>
							</div>
							<h6 class="tve_p_center"
							    style="color: #333;font-size: 22px;margin-top: 0;margin-bottom: 30px;">
								Detailed <br>Lab Reports
							</h6>
							<p class="tve_p_center"
							   style="font-size: 18px;margin-top: 0;margin-bottom: 40px;">
								Our in-house lab offers fast access to detailed investigations
							</p>
						</div>
						<div class="tve_colm tve_foc tve_lst">
							<div class="thrv_wrapper thrv_icon aligncenter"
							     style="font-size: 50px; margin-bottom: 20px;">
								<span data-tve-icon="bizmedical-icon-briefcase"
								      class="tve_sc_icon bizmedical-icon-briefcase tve_blue"></span>
							</div>
							<h6 class="tve_p_center"
							    style="color: #333;font-size: 22px;margin-top: 0;margin-bottom: 30px;">
								High Class <br>Treatments
							</h6>
							<p class="tve_p_center"
							   style="font-size: 18px;margin-top: 0;margin-bottom: 40px;">
								We offer fair professional medical advice and the newest treatments
								at the highest standards
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1">
		<div class="out" style="background-color: #f8f8f8;">
			<div class="in darkSec">
				<div class="cck clearfix">
					<h2 class="tve_p_center rft"
					    style="color: #3868c4;font-size: 36px;margin-top:20px;margin-bottom: 50px;">
						Our Medical Specialties
					</h2>
					<div class="thrv_wrapper thrv_columns tve_clearfix">
						<div class="tve_colm tve_oth">
							<ul class="thrv_wrapper">
								<li>Proin gravida nibh vel biben</li>
								<li>Aenean sollicitudin</li>
								<li>Sollicitudin, lorem quis</li>
							</ul>
						</div>
						<div class="tve_colm tve_oth">
							<ul class="thrv_wrapper">
								<li>Sollicitudin, lorem quis biben</li>
								<li>Consequat ipsum</li>
								<li>Trom nibh id elit</li>
							</ul>
						</div>
						<div class="tve_colm tve_thc tve_lst">
							<ul class="thrv_wrapper">
								<li>Proin gravida nibh vel biben</li>
								<li>Aenean sollicitudin</li>
								<li>Sollicitudin, lorem quis</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1">
		<div class="out" style="background-color: #4c8aca;">
			<div class="in darkSec">
				<div class="cck clearfix">
					<div class="thrv_wrapper thrv_content_container_shortcode">
						<div class="tve_clear"></div>
						<div class="tve_center tve_content_inner"
						     style="width: 440px;min-width:50px; min-height: 2em;margin-top: -75px;">
							<div class="thrv_wrapper thrv_button_shortcode tve_fullwidthBtn"
							     data-tve-style="1">
								<div class="tve_btn tve_green tve_normalBtn tve_btn7 tve_nb">
									<a class="tve_btnLink" href="">
				                        <span class="tve_left tve_btn_im">
				                            <i></i>
				                        </span>
										<span class="tve_btn_txt">Make an Appointment</span>
									</a>
								</div>
							</div>
							<h6 class="tve_p_center"
							    style="color: #fff; font-size: 24px;margin-top: 0;margin-bottom: 40px;">
								<span class="bold_text">
									or CALL : 0745 123 456
								</span>
							</h6>
						</div>
						<div class="tve_clear"></div>
					</div>
					<h2 class="tve_p_center rft"
					    style="color: #fff; font-size: 36px;margin-top: 0;margin-bottom: 30px;">
						What Our Patients Have to Say
					</h2>
					<div class="thrv_wrapper thrv_contentbox_shortcode" data-tve-style="5">
						<div class="tve_cb tve_cb5 tve_white">
							<div class="tve_cb_cnt">
								<div class="thrv_wrapper thrv_testimonial_shortcode"
								     data-tve-style="4">
									<div class="tve_ts tve_ts4 tve_white">
										<div class="tve_ts_o">
											<div class="tve_ts_imc">
												<img
													src="{tcb_lp_base_url}/css/images/knowhow-person2.png"
													alt=""/>
											</div>
							                <span>
							                    <b>Shane Melaugh</b>
							                </span>
										</div>
										<div class="tve_ts_t">
											<span class="tve_ts_c tve_left"></span>

											<div class="tve_ts_cn tve_left">
												<span class="tve_ts_ql"></span>
												<p style="color: #363636; font-size: 18px;margin-top: 0;margin-bottom: 10px;">
													Excepteur sint occaecat cupidatat non
													proident,
													sunt in culpa qui officia deserunt
													mollit anim
													id est laborum. Sed ut perspiciatis unde
													omnis
													iste natus error sit voluptatem
													accusantium
													doloremque laudantium.
												</p>
												<p style="color: #3868c4; font-size: 18px;margin-top: 0;margin-bottom: 0;">
													Alice Thinkerton
												</p>
											</div>
											<div class="tve_clear"></div>
										</div>
										<div class="tve_clear"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="thrv_wrapper thrv_contentbox_shortcode" data-tve-style="5">
						<div class="tve_cb tve_cb5 tve_white" style="margin-bottom: 30px;">
							<div class="tve_cb_cnt">
								<div class="thrv_wrapper thrv_testimonial_shortcode"
								     data-tve-style="4">
									<div class="tve_ts tve_ts4 tve_white">
										<div class="tve_ts_o">
											<div class="tve_ts_imc">
												<img
													src="{tcb_lp_base_url}/css/images/knowhow-person4.png"
													alt=""/>
											</div>
							                <span>
							                    <b>Shane Melaugh</b>
							                </span>
										</div>
										<div class="tve_ts_t">
											<span class="tve_ts_c tve_left"></span>

											<div class="tve_ts_cn tve_left">
												<span class="tve_ts_ql"></span>
												<p style="color: #363636; font-size: 18px;margin-top: 0;margin-bottom: 10px;">
													Excepteur sint occaecat cupidatat non
													proident,
													sunt in culpa qui officia deserunt
													mollit anim
													id est laborum. Sed ut perspiciatis unde
													omnis
													iste natus error sit voluptatem
													accusantium
													doloremque laudantium.
												</p>
												<p style="color: #3868c4; font-size: 18px;margin-top: 0;margin-bottom: 0;">
													Cameron Alastair
												</p>
											</div>
											<div class="tve_clear"></div>
										</div>
										<div class="tve_clear"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1">
		<div class="out" style="background-color: #fff;">
			<div class="in darkSec">
				<div class="cck clearfix">
					<h2 class="tve_p_center rft"
					    style="color: #3868c4; font-size: 36px;margin-top: 20px;margin-bottom: 20px;">
						Our Medical Team
					</h2>
					<div class="thrv_wrapper thrv_content_container_shortcode">
						<div class="tve_clear"></div>
						<div class="tve_center tve_content_inner"
						     style="width: 840px;min-width:50px; min-height: 2em;">
							<p class="tve_p_center"
							   style="color: #333;font-size:18px;margin-top: 0;margin-bottom: 20px;">
								<span class="bold_text">
									You can be good in many specialties but it’s hard to be exceptional at all.
									Each of our team members is trained in a special branch of medicine.
								</span>
							</p>
						</div>
						<div class="tve_clear"></div>
					</div>
					<div class="thrv_wrapper thrv_columns tve_clearfix">
						<div class="tve_colm tve_foc">
							<div style="margin-top: 0;margin-bottom: 0;width: 190px;"
							     class="thrv_wrapper tve_image_caption img_style_circle aligncenter">
		                        <span class="tve_image_frame">
		                            <img class="tve_image"
		                                 src="{tcb_lp_base_url}/css/images/bizmedical-image1.jpg"
		                                 style="width: 197px"/>
		                        </span>
							</div>
							<p class="tve_p_center"
							   style="color: #333; font-size: 18px;margin-top: 30px;margin-bottom: 0;">
								<span class="bold_text">
									DR. JOHN MCKINTIRE
								</span>
							</p>
							<div class="thrv_wrapper" style="margin-top: 0;margin-bottom: 0;">
								<hr class="tve_sep tve_sep1"/>
							</div>
							<p class="tve_p_center"
							   style="color: #333333; font-size: 18px;margin-top: 0;margin-bottom: 20px;">
								Cardiologist
							</p>
						</div>
						<div class="tve_colm tve_foc">
							<div style="margin-top: 0;margin-bottom: 0;width: 190px;"
							     class="thrv_wrapper tve_image_caption img_style_circle aligncenter">
		                        <span class="tve_image_frame">
		                            <img class="tve_image"
		                                 src="{tcb_lp_base_url}/css/images/bizmedical-image2.jpg"
		                                 style="width: 197px"/>
		                        </span>
							</div>
							<p class="tve_p_center"
							   style="color: #333; font-size: 18px;margin-top: 30px;margin-bottom: 0;">
								<span class="bold_text">
									DR. MARY NEWVILLE
								</span>
							</p>
							<div class="thrv_wrapper" style="margin-top: 0;margin-bottom: 0;">
								<hr class="tve_sep tve_sep1"/>
							</div>
							<p class="tve_p_center"
							   style="color: #333333; font-size: 18px;margin-top: 0;margin-bottom: 20px;">
								Rheumatologist
							</p>
						</div>
						<div class="tve_colm tve_foc">
							<div style="margin-top: 0;margin-bottom: 0;width: 190px;"
							     class="thrv_wrapper tve_image_caption img_style_circle aligncenter">
		                        <span class="tve_image_frame">
		                            <img class="tve_image"
		                                 src="{tcb_lp_base_url}/css/images/bizmedical-image3.jpg"
		                                 style="width: 197px"/>
		                        </span>
							</div>
							<p class="tve_p_center"
							   style="color: #333; font-size: 18px;margin-top: 30px;margin-bottom: 0;">
								<span class="bold_text">
									DR. ANDREEA KESSLER
								</span>
							</p>
							<div class="thrv_wrapper" style="margin-top: 0;margin-bottom: 0;">
								<hr class="tve_sep tve_sep1"/>
							</div>
							<p class="tve_p_center"
							   style="color: #333333; font-size: 18px;margin-top: 0;margin-bottom: 20px;">
								Laboratory Manager
							</p>
						</div>
						<div class="tve_colm tve_foc tve_lst">
							<div style="margin-top: 0;margin-bottom: 0;width: 190px;"
							     class="thrv_wrapper tve_image_caption img_style_circle aligncenter">
		                        <span class="tve_image_frame">
		                            <img class="tve_image"
		                                 src="{tcb_lp_base_url}/css/images/bizmedical-image4.jpg"
		                                 style="width: 197px"/>
		                        </span>
							</div>
							<p class="tve_p_center"
							   style="color: #333; font-size: 18px;margin-top: 30px;margin-bottom: 0;">
								<span class="bold_text">
									DR. NANCY DEWMORE
								</span>
							</p>
							<div class="thrv_wrapper" style="margin-top: 0;margin-bottom: 0;">
								<hr class="tve_sep tve_sep1"/>
							</div>
							<p class="tve_p_center"
							   style="color: #333333; font-size: 18px;margin-top: 0;margin-bottom: 20px;">
								Neurologist
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1">
		<div class="out" style="background-color: #4c8aca;">
			<div class="in darkSec">
				<div class="cck clearfix">
					<div class="thrv_wrapper thrv_columns">
						<div class="tve_colm tve_twc">
							<h2 style="color: #f9f9f9;font-size:36px;margin-top: 0;margin-bottom: 30px;" class="rft">
								Make Your Appointment Today
							</h2>
							<p style="color: #f9f9f9;font-size: 18px;margin-top: 0;margin-bottom: 30px;">
								<span class="bold_text">
									Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin,
									lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis
									sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit
									amet mauris. Morbi accumsan ipsum velit.
								</span>
							</p>
							<div class="thrv_wrapper thrv_content_container_shortcode bizmedical-container">
								<div class="tve_clear"></div>
								<div class="tve_left tve_content_inner" style="width: 450px;min-width:50px; min-height: 2em;">
									<div
										class="thrv_wrapper thrv_button_shortcode tve_fullwidthBtn"
										data-tve-style="1">
										<div
											class="tve_btn tve_green tve_normalBtn tve_btn7 tve_nb">
											<a class="tve_btnLink" href="">
					                                            <span class="tve_left tve_btn_im">
					                                                <i></i>
					                                            </span>
												<span class="tve_btn_txt">Make an Appointment</span>
											</a>
										</div>
									</div>
									<h6 class="tve_p_center" style="color: #fff;font-size: 24px;margin-top: 0;margin-bottom: 0;">
										<span class="bold_text">
											or CALL : 0745 123 456
										</span>
									</h6>
								</div>
								<div class="tve_clear"></div>
							</div>
						</div>
						<div class="tve_colm tve_twc tve_lst">
							<div style="margin-top: 20px;margin-bottom: 0;width: 452px;"
							     class="thrv_wrapper tve_image_caption img_style_framed bizmedical-image">
		                        <span class="tve_image_frame">
		                            <img class="tve_image"
		                                 src="{tcb_lp_base_url}/css/images/bizmedical-image5.jpg"
		                                 style="width: 452px"/>
		                        </span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="tve_lp_footer tve_empty_dropzone tve_content_width">
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1">
		<div class="out" style="background-color: #4c8aca;margin-top: 1px;">
			<div class="in darkSec">
				<div class="cck clearfix">
					<div class="thrv_wrapper thrv_columns" style="margin-top: 10px;margin-bottom: 10px;">
						<div class="tve_colm tve_twc">
							<p style="color: #ffffff; font-size: 20px;margin-top: 0;margin-bottom: 0;">
								<span class="bold_text">
									Copyright {tcb_current_year} - Company Name
								</span>
							</p>
						</div>
						<div class="tve_colm tve_twc tve_lst">
							<p style="color: #ffffff;font-size: 20px;margin-top: 0;margin-bottom: 0;"
							   class="tve_p_right">
								<span class="bold_text">
									<a href="#">Disclaimer</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="#">Contact</a>
								</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>