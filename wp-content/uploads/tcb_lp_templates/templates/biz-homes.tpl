<div class="tve_lp_header tve_content_width tve_empty_dropzone">
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1">
		<div class="out" style="background-color: #fff;box-shadow: 0 4px 0 0 #e6e6e6;">
			<div class="in darkSec">
				<div class="cck clearfix">
					<div class="thrv_wrapper thrv_columns" style="margin-top: 0;margin-bottom: 0;">
						<div class="tve_colm tve_twc">
							<div style="margin-top: 0;margin-bottom: 0;width: 213px;"
							     class="thrv_wrapper tve_image_caption bizhomes-logo">
		                        <span class="tve_image_frame">
		                            <img class="tve_image"
		                                 src="{tcb_lp_base_url}/css/images/bizhomes-logo.png"
		                                 style="width: 213px"/>
		                        </span>
							</div>
						</div>
						<div class="tve_colm tve_twc tve_lst">
							<div class="thrv_wrapper thrv_content_container_shortcode bizhomes-text">
								<div class="tve_clear"></div>
								<div class="tve_right tve_content_inner" style="width: 180px;min-width:50px; min-height: 2em;">
									<h6 class="bizhomes-phone" style="color: #333;font-size: 18px;line-height: 1.33em;margin-top: 0;margin-bottom: 0;">
										<span class="bold_text">
											<font color="#008cd9">Call us:</font> <br>
											0723 456 789
										</span>
									</h6>
								</div>
								<div class="tve_clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="tve_lp_content tve_editor_main_content tve_empty_dropzone tve_content_width">
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1" style="margin-top: 4px;">
		<div class="out">
			<div class="in lightSec pddbg" data-width="1920" data-height="651"
			     style="box-sizing: border-box; background-image: url({tcb_lp_base_url}/css/images/bizhomes-ps1.jpg);">
				<div class="cck clearfix">
					<div class="thrv_wrapper thrv_columns tve_clearfix" style="margin-bottom: 0;">
						<div class="tve_colm tve_tth">
							<p>
								&nbsp;
							</p>
							<div class="thrv_wrapper thrv_content_container_shortcode bizhomes-title">
								<div class="tve_clear"></div>
								<div class="tve_right tve_content_inner" style="width: 600px;min-width:50px; min-height: 2em;">
									<h1 style="color: #fff; font-size: 50px;margin-top: 0;margin-bottom: 0;" class="tve_p_right bizhomes-title-heading rft">
										Find your <span class="bold_text">Dream Home</span> <br>
										For the <span class="bold_text">Right Price</span>
									</h1>
								</div>
								<div class="tve_clear"></div>
							</div>
						</div>
						<div class="tve_colm tve_oth tve_lst">
							<div class="thrv_wrapper thrv_contentbox_shortcode" data-tve-style="5">
								<div class="tve_cb tve_cb5 tve_blue" style="margin-bottom: -85px;">
									<div class="tve_cb_cnt">
										<h4 class="tve_p_center" style="color: #fff; font-size: 26px;margin-top: 0;margin-bottom: 20px;">
											Request a Consultation <br>
											to Find Your Dream Home
										</h4>
										<h6 style="color: #fff; font-size: 18px;margin-top: 0;margin-bottom: 20px;" class="tve_p_center">
											We’ll help you find the right house that
											suits your lifestyle and budget.
										</h6>
										<div
											class="thrv_wrapper thrv_lead_generation tve_clearfix tve_orange tve_3 thrv_lead_generation_vertical tve_centerBtn"
											data-inputs-count="3" data-tve-style="1"
											style="margin-top: 20px;margin-bottom: 20px;max-width: 270px;">
											<div class="thrv_lead_generation_code"
											     style="display: none;"></div>
											<div
												class="thrv_lead_generation_container tve_clearfix">
												<div
													class="tve_lead_generated_inputs_container tve_clearfix">
													<div class="tve_lead_fields_overlay"></div>
													<div
														class=" tve_lg_input_container tve_lg_3 tve_lg_input">
														<input type="text"
														       data-placeholder="Your Name"
														       placeholder="Your Name"
														       value=""
														       name="name"/>
													</div>
													<div
														class=" tve_lg_input_container tve_lg_3 tve_lg_input">
														<input type="email"
														       data-placeholder="Your Email"
														       placeholder="Your name"
														       value=""
														       name="email"/>
													</div>
													<div
														class=" tve_lg_input_container tve_lg_3 tve_lg_input">
														<input type="text"
														       data-placeholder="Your Phone"
														       placeholder="Your Phone"
														       value=""
														       name="phone"/>
													</div>
													<div
														class="tve_lg_input_container tve_submit_container tve_lg_3 tve_lg_submit">
														<button type="Submit">Get Started Now !
														</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1">
		<div class="out" style="background-color: #f9f9f9;">
			<div class="in darkSec">
				<div class="cck clearfix">
					<div class="thrv_wrapper thrv_columns tve_clearfix" style="margin-top: 80px;">
						<div class="tve_colm tve_tth">
							<h2 style="color: #008cd9; font-size: 36px;margin-top: 0;margin-bottom: 0;" class="rft">
								Welcome
							</h2>
							<div class="thrv_wrapper" style="margin-top: 0;margin-bottom: 30px;">
								<hr class="tve_sep tve_sep1"/>
							</div>
							<p style="color: #000; font-size: 18px;margin-top: 0;margin-bottom: 30px;">
								Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin,
								lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis
								sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit
								amet mauris. Morbi accumsan ipsum velit.
							</p>
							<p style="color: #000; font-size: 18px;margin-top: 0;margin-bottom: 30px;">
								Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris
								vitae erat consequat auctor eu in elit. himenaeos.
							</p>
						</div>
						<div class="tve_colm tve_oth tve_lst">
							<div
								style="width: 456px;margin-top: 25px;margin-bottom: 0;"
								class="thrv_wrapper tve_image_caption aligncenter">
                                <span class="tve_image_frame">
                                    <img class="tve_image"
                                         src="{tcb_lp_base_url}/css/images/bizhomes-image1.jpg"
                                         style="width: 456px"/>
                                </span>
								<p class="wp-caption-text" style="color: #666666;font-size: 14px;margin-top: 0;margin-bottom: 0;">
									This can be a video presentation or an image
								</p>
							</div>
						</div>
					</div>
					<div class="thrv_wrapper thrv_columns tve_clearfix">
						<div class="tve_colm tve_oth">
							<div class="thrv_wrapper thrv_contentbox_shortcode" data-tve-style="5">
								<div class="tve_cb tve_cb5 tve_white">
									<div class="tve_cb_cnt">
										<div class="thrv_wrapper thrv_icon aligncenter"
										     style="font-size: 50px;margin-top: 20px;">
												<span data-tve-icon="bizhomes-icon-study"
												      class="tve_sc_icon bizhomes-icon-study tve_blue"></span>
										</div>
										<h5 class="tve_p_center" style="color: #008cd9; font-size:  24px;margin-top: 20px;margin-bottom: 30px;">
											10 years experience
										</h5>
										<p style="color: #666; font-size: 18px;margin-top: 0;margin-bottom: 20px;" class="tve_p_center">
											Proin gravida nibh vel velit auctor aliquet. Aenean
											sollicitudin,
											lorem
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="tve_colm tve_oth">
							<div class="thrv_wrapper thrv_contentbox_shortcode" data-tve-style="5">
								<div class="tve_cb tve_cb5 tve_white">
									<div class="tve_cb_cnt">
										<div class="thrv_wrapper thrv_icon aligncenter"
										     style="font-size: 50px;margin-top: 20px;">
												<span data-tve-icon="bizhomes-icon-shop"
												      class="tve_sc_icon bizhomes-icon-shop tve_blue"></span>
										</div>
										<h5 class="tve_p_center" style="color: #008cd9; font-size:  24px;margin-top: 20px;margin-bottom: 30px;">
											Complete Services
										</h5>
										<p style="color: #666; font-size: 18px;margin-top: 0;margin-bottom: 20px;" class="tve_p_center">
											Aenean sollicitudin, lorem quis bibendum auctor, nisi
											elit
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="tve_colm tve_thc tve_lst">
							<div class="thrv_wrapper thrv_contentbox_shortcode" data-tve-style="5">
								<div class="tve_cb tve_cb5 tve_white">
									<div class="tve_cb_cnt">
										<div class="thrv_wrapper thrv_icon aligncenter"
										     style="font-size: 50px;margin-top: 20px;">
												<span data-tve-icon="bizhomes-icon-banknote"
												      class="tve_sc_icon bizhomes-icon-banknote tve_blue"></span>
										</div>
										<h5 class="tve_p_center" style="color: #008cd9; font-size:  24px;margin-top: 20px;margin-bottom: 30px;">
											Best Price Guarantee
										</h5>
										<p style="color: #666; font-size: 18px;margin-top: 0;margin-bottom: 20px;" class="tve_p_center">
											Nam nec tellus a odio tincidunt auctor a ornare odio
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<h2 class="tve_p_center rft" style="color: #008cd9; font-size: 36px;margin-top: 60px;margin-bottom: 0;">
						Property Types in Our Offer
					</h2>
					<div class="thrv_wrapper" style="margin-top: 0;margin-bottom: 30px;">
						<hr class="tve_sep tve_sep2"/>
					</div>
					<div class="thrv_wrapper thrv_contentbox_shortcode" data-tve-style="6">
						<div class="tve_cb tve_cb6 tve_white">
							<div class="tve_cb_cnt">
								<div class="thrv_wrapper thrv_content_container_shortcode">
									<div class="tve_clear"></div>
									<div class="tve_center tve_content_inner" style="width: 700px;min-width:50px; min-height: 2em;">
										<h5 class="tve_p_center" style="color: #333; font-size: 24px;margin-top: 10px;margin-bottom: 50px;">
											We’ll Find Your Dream Home in Our Wide Range of Properties
										</h5>
										<p class="tve_p_center" style="color: #666;font-size: 16px;margin-top: 0;margin-bottom: 40px;">
											Sed non mauris vitae erat consequat auctor Duis sed odio sit
											amet nibh vulputate cursus a sit amet mauris. ornare odio. eu in
											elit. himenaeos.
											Mauris in erat justo. Nullam ac urna eu felis dapibus
											condimentum sit amet a augue. Sed non
										</p>
									</div>
									<div class="tve_clear"></div>
								</div>
								<div class="thrv_wrapper thrv_post_grid"
								     style="margin-bottom: 0;">
									<div class="thrive-shortcode-config"
									     style="display: none !important">__CONFIG_post_grid__{"tve_lb_type":"tve_post_grid","text_type":"summary","post_types":{"post":"true","page":"false","attachment":"false","product":"false","product_variation":"false","shop_order":"false","shop_order_refund":"false","shop_coupon":"false","shop_webhook":"false"},"posts_per_page":"3","posts_start":0,"orderby":"date","order":"DESC","recent_days":"0","filters":{"category":"","tag":"","tax":"","author":"","posts":""},"columns":"3","display":"grid","layout":["title","featured_image","text"],"action":"tve_do_post_grid_shortcode","teaser_layout":{"featured_image":"true","title":"false","text":"false","read_more":"false"},"grid_layout":"horizontal"}__CONFIG_post_grid__</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1" style="margin-top: -150px;">
		<div class="out" style="background-color: #008cd9;">
			<div class="in ligthSec" style="padding-top: 100px;">
				<div class="cck clearfix">
					<div class="thrv_wrapper thrv_content_container_shortcode">
						<div class="tve_clear"></div>
						<div class="tve_center tve_content_inner" style="width: 480px;min-width:50px; min-height: 2em;">
							<h2 class="tve_p_center rft" style="color: #fff;font-size: 36px;margin-top: 30px;margin-bottom: 0;">
								Let’s Find <span class="bold_text">Your Dream Home</span>
							</h2>
							<div class="thrv_wrapper" style="margin-top: 10px;margin-bottom: 30px;">
								<hr class="tve_sep tve_sep3"/>
							</div>
							<p class="tve_p_center" style="color: #fff; font-size: 16px;line-height: 1.875em;margin-top: 0;margin-bottom: 40px;">
								Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi
								accumsan ipsum velit.
							</p>
							<div
								class="thrv_wrapper thrv_button_shortcode tve_centerBtn"
								data-tve-style="1">
								<div
									class="tve_btn tve_orange tve_normalBtn tve_btn7 tve_nb"
									style="">
									<a class="tve_btnLink tve_evt_manager_listen" href=""  data-tcb-events="{tcb_open_lightbox}">
	                            <span class="tve_left tve_btn_im">
	                               <i></i>
	                            </span>
								<span
									class="tve_btn_txt">Get Started  Now !</span>
									</a>
								</div>
							</div>
						</div>
						<div class="tve_clear"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="thrv_wrapper thrv_page_section" data-tve-style="1">
		<div class="out" style="background-color: #eeeeee;">
			<div class="in darkSec">
				<div class="cck clearfix">
					<h2 style="color: #008cd9; font-size: 36px;margin-top: 20px;margin-bottom: 50px;"
					    class="rft tve_p_center">
						What our Customers have to say:
					</h2>
					<div class="thrv_wrapper thrv_testimonial_shortcode"
					     data-tve-style="4" style="margin-bottom: 40px;">
						<div class="tve_ts tve_ts4 tve_white">
							<div class="tve_ts_o">
								<div class="tve_ts_imc">
									<img
										src="{tcb_lp_base_url}/css/images/knowhow-person3.png"
										alt=""/>
								</div>
							                <span>
							                    <b>Shane Melaugh</b>
							                </span>
							</div>
							<div class="tve_ts_t">
								<span class="tve_ts_c tve_left"></span>

								<div class="tve_ts_cn tve_left">
									<span class="tve_ts_ql"></span>
									<p class="italic_text"
									   style="color: #1288cc; font-size: 30px;margin-top: 0;margin-bottom: 20px;">
										“Best part of the review will be used as a title here”
									</p>
									<p style="color: #333; font-size: 20px;margin-top: 0;margin-bottom: 20px;">
										<span class="bold_text italic_text">
											“Vivamus sit amet lacus eu odio lacinia efficitur venenatis quis tellus.
											Ut eget lacus velit. Sed egestas diam vel iaculis dapibus. Fusce tortor lorem,
											fringilla et tortor eu, hendrerit convallis. Pellentesque non facilisis purus, id lorem ipsum  ultrices erat.
										</span>
									</p>
									<p style="color: #1288cc; font-size: 20px;line-height: 1.5em;margin-top: 0;margin-bottom: 0;">
										<span class="italic_text">
											Albert Stewart <br>
											<font color="#414141">Professional Testimonial
												poser</font>
										</span>
									</p>
								</div>
								<div class="tve_clear"></div>
							</div>
							<div class="tve_clear"></div>
						</div>
					</div>
					<div class="thrv_wrapper thrv_testimonial_shortcode"
					     data-tve-style="4" style="margin-bottom: 30px;">
						<div class="tve_ts tve_ts4 tve_white">
							<div class="tve_ts_o">
								<div class="tve_ts_imc">
									<img
										src="{tcb_lp_base_url}/css/images/knowhow-person2.png"
										alt=""/>
								</div>
							                <span>
							                    <b>Shane Melaugh</b>
							                </span>
							</div>
							<div class="tve_ts_t">
								<span class="tve_ts_c tve_left"></span>

								<div class="tve_ts_cn tve_left">
									<span class="tve_ts_ql"></span>
									<p class="italic_text"
									   style="color: #1288cc; font-size: 30px;margin-top: 0;margin-bottom: 20px;">
										“Best part of the review will be used as a title here”
									</p>
									<p style="color: #333; font-size: 20px;margin-top: 0;margin-bottom: 20px;">
										<span class="bold_text italic_text">
											“Vivamus sit amet lacus eu odio lacinia efficitur venenatis quis tellus. Ut eget lacus velit. Sed egestas diam vel iaculis dapibus. nubia nostra, per inceptos himenaeos. ”
										</span>
									</p>
									<p style="color: #1288cc; font-size: 20px;line-height: 1.5em;margin-top: 0;margin-bottom: 0;">
										<span class="italic_text">
											Samantha Allen <br>
											<font color="#414141">Professional Testimonial
												poser</font>
										</span>
									</p>
								</div>
								<div class="tve_clear"></div>
							</div>
							<div class="tve_clear"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="tve_lp_footer tve_empty_dropzone tve_content_width">
	<div class="thrv_wrapper thrv_columns">
		<div class="tve_colm tve_twc">
			<p style="color: #333333; font-size: 20px;margin-top: 0;margin-bottom: 0;">
				Copyright {tcb_current_year} - Company Name
			</p>
		</div>
		<div class="tve_colm tve_twc tve_lst">
			<p style="color: #333333;font-size: 20px;margin-top: 0;margin-bottom: 0;" class="tve_p_right">
				<a href="#">Disclaimer</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="#">Contact</a>
			</p>
		</div>
	</div>
</div>